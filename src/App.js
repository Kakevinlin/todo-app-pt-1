import React, { Component } from "react";
import todosList from "./todos.json";
import TodoList from './Components/ToDoList/ToDoList.js'
import { v4 as uuidv4 } from 'uuid';



// {
//   "userId": 1,
//   "id": 5,
//   "title": "laboriosam mollitia et enim quasi adipisci quia provident illum",
//   "completed": false
// }

class App extends Component {
  state = {
    todos: todosList,
    typedtext: ""
  };
  
  handleTypedtext=(event)=>{
    this.setState({typedtext: event.target.value})
  }

  addtodo = (todo)=>{
   this.setState({todos:[ ...this.state.todos,todo]})
  }

  handlekeypress=(event)=>{
    event.preventDefault()
    // if (event.keycode === 13){
      const newtodo = {
          "userId": 1,
          "id": uuidv4(),
          "title":this.state.typedtext,
          "completed": false
        }
        this.addtodo(newtodo)
    // }
  }

  // change "id"
  // add newtodo to state
  // copy old state change to newtodo 
  handleDelete=(todoid)=>{
    const newtodos=this.state.todos.filter(
     randomvariable=>(randomvariable.id!==todoid)
    )

    this.setState({todos:newtodos})
  }
  handleDeleteAllCompleted=()=>{
    const newtodos=this.state.todos.filter(
     randomvariable=>(randomvariable.completed===false)
    )

    this.setState({todos:newtodos})
  }
  handleComplete=(todoid)=>{
    const newtodos=this.state.todos.map(
      randomvariable=>{
        if(randomvariable.id === todoid)
        {
          randomvariable.completed = !randomvariable.completed
        }
        return randomvariable
      }
     )
 
     this.setState({todos:newtodos})  
    }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={this.handlekeypress}>
          <input className="new-todo" placeholder="What needs to be done?" autoFocus 
          onChange={this.handleTypedtext} value={this.state.typedtext}/>
          </form>
        </header>
        <TodoList todos={this.state.todos} handleDelete={this.handleDelete} handleComplete={this.handleComplete}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={()=> this.handleDeleteAllCompleted()}>Clear completed</button>
        </footer>
      </section>
    );
  }
}





export default App;
