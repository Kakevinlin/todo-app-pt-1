import React, { Component } from "react";

class TodoItem extends Component {

    
    render() {
      return (
        <li className={this.props.completed ? "completed" : ""}>
          <div className="view">
            <input className="toggle" type="checkbox" onChange={()=> this.props.handleComplete(this.props.id)} checked={this.props.completed} />
            <label>{this.props.title}</label>
            <button className="destroy" onClick={()=> this.props.handleDelete(this.props.id)} />
          </div>
        </li>
      );
    }
  }

  export default TodoItem;