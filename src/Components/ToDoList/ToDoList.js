import React, { Component } from "react";
import TodoItem from '../ToDoItem/ToDoItem.js'

class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map((todo,i) => (
              <TodoItem title={todo.title} completed={todo.completed}
              key={i}
              handleDelete={this.props.handleDelete} id={todo.id}
              handleComplete={this.props.handleComplete}  />
            ))}
          </ul>
        </section>
      );
    }
  }

  export default TodoList;